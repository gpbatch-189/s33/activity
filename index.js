
// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')

.then((response) => response.json())
.then((data) => console.log(data))


// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title;
	}));
});


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/1')
	
.then((response) => response.json())
.then((data) => console.log(data))


// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((title) => console.log(title))


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.


fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title:'New To Do 1',
		completed: false		
	})
})
	
.then((response) => response.json())
.then((data) => console.log(data))


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

/* 
	9. Update a to do list item by changing the data structure to contain the following properties:
	a. Title
	b. Description
	c. Status
	d. Date Completed
	e. User ID
*/	


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title:'New To Do 2',
		description: 'Pay your bills',
		status: 'Pending',
		userId: 1
	})
})
	
.then((response) => response.json())
.then((data) => console.log(data))

// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title:'New To Do 3',
		description: 'Cook Rice',
		status: 'Complete'	
	})
})
	
.then((response) => response.json())
.then((data) => console.log(data))


// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.


fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))


